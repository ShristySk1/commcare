package com.ayata.commcare;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


public class FragmentPatientAddress extends Fragment implements AdapterAddress.SingleClickListener {
RecyclerView recyclerView;
AdapterAddress adapter;
int layout;
List<ModelAddress> list;
ModelAddress modelAddress;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
  View view= inflater.inflate(R.layout.fragment_fragment_patient_address, container, false);
  recyclerView=view.findViewById(R.id.recycle_container);
  list=new ArrayList<>();
        DataPrepare();

        layout=R.layout.activity_layout_designation_single;

        adapter=new AdapterAddress(list,layout);

        adapter.setOnItemClickListener(this);
  RecyclerView.LayoutManager manager= new LinearLayoutManager(getContext());
  recyclerView.setLayoutManager(manager);
  recyclerView.setAdapter(adapter);


  return  view;
    }

void DataPrepare(){
        list.add(new ModelAddress("Ward no. 1"));
    list.add(new ModelAddress("Ward no. 2"));

    list.add(new ModelAddress("Ward no. 3"));


}
    @Override
    public void onItemClickListener(int position) {
        adapter.selectedItem(position);
    }
}

