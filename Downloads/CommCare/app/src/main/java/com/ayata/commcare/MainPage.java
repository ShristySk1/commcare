package com.ayata.commcare;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

public class MainPage extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page);
//        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_page,new FragmentHome(),null).commit();
        loadFragment(new FragmentHome());
        BottomNavigationView bottom_nav=findViewById(R.id.bottom_navigation);
        bottom_nav.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selected_fragment=null;
        switch(item.getItemId()){
            case R.id.nav_home:
                selected_fragment=new FragmentHome();
                break;
            case R.id.nav_gallary:
                selected_fragment=new FragmentCategory();
                break;
            case R.id.nav_analytics:
                selected_fragment=new FragmentPatientAddress();
                break;
        }
        return loadFragment(selected_fragment);
    }
    private boolean loadFragment(Fragment fragment){
        if(fragment!=null){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main_page,fragment,null).commit();
return true;
        }
        return false;
    }
}
