package com.ayata.commcare;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


public class FragmentHome extends Fragment {
    RecyclerView recyclerView;
    AdapterHome adapter;
    List<ModelHomeBox> list  =new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
   View view= inflater.inflate(R.layout.fragment_fragment_home, container, false);
   recyclerView=view.findViewById(R.id.recycle_container);
   RecyclerView.LayoutManager manager=new LinearLayoutManager(getContext());
   ((LinearLayoutManager) manager).setOrientation(LinearLayoutManager.HORIZONTAL);
   adapter=new AdapterHome(list,getContext());
   recyclerView.setLayoutManager(manager);
   recyclerView.setAdapter(adapter);
 DataPrepare();
        return  view;
    }
public void DataPrepare(){

        list.add(new ModelHomeBox(2600,"header1","paragraph1"));
    list.add(new ModelHomeBox(1600,"header2","paragraph2"));
    list.add(new ModelHomeBox(00,"header3","paragraph3"));
}

}
