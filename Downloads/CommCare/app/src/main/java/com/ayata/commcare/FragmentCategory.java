package com.ayata.commcare;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

import java.util.ArrayList;
import java.util.List;


public class FragmentCategory extends Fragment implements AdapterCategory.CategoryClickListener {
RecyclerView recyclerView1,recyclerView2;
List<ModelCategoryBox> list1,list2;
AdapterCategory adapter,adapter2;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_category, container, false);
       recyclerView1=view.findViewById(R.id.recycle_container1);
        recyclerView2=view.findViewById(R.id.recycle_container2);

        list1=new ArrayList<>();
        list2=new ArrayList<>();

        LinearLayoutManager manager=new LinearLayoutManager(getContext());
       ((LinearLayoutManager) manager).setOrientation(RecyclerView.HORIZONTAL);
        LinearLayoutManager manager2=new LinearLayoutManager(getContext());
        ((LinearLayoutManager) manager2).setOrientation(RecyclerView.HORIZONTAL);

       adapter=new AdapterCategory(list1,getContext(),this);
        recyclerView1.setLayoutManager(manager);
        recyclerView1.setAdapter(adapter);


        adapter2=new AdapterCategory(list2,getContext(),this);
        recyclerView2.setLayoutManager(manager2);
        recyclerView2.setAdapter(adapter2);
       DataPrepare();

       return  view;
    }
public void DataPrepare(){

        list1.add(new ModelCategoryBox("header1","paragraph1",R.drawable.ad2outer));
    list1.add(new ModelCategoryBox("header2","paragraph2",R.drawable.ad2outer2));
    list2.add(new ModelCategoryBox("header1","paragraph1",R.drawable.ad2outer3));
    list2.add(new ModelCategoryBox("header2","paragraph2",R.drawable.ad2outer4));
}

    @Override
    public void SingleCategoryClick(int position) {
        Intent intent=new Intent(getContext(),Login.class);
        startActivity(intent);
    }
}
