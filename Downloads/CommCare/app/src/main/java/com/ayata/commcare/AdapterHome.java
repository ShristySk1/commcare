package com.ayata.commcare;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdapterHome extends RecyclerView.Adapter<AdapterHome.HomeViewHolder> {
    Context context;
static GradientDrawable gradientDrawable;
    static int[] color;

    List<ModelHomeBox> list=new ArrayList<>();

    public AdapterHome(List<ModelHomeBox> list,Context context) {
        this.context=context;
color=context.getResources().getIntArray(R.array.rainbow);
        this.list = list;
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_box_single,parent,false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        gradientDrawable.setColor(color[position]);
//holder.relativeLayout.setBackgroundColor(color[1]);
holder.header.setText(list.get(position).getHeader());
        holder.paragraph.setText(list.get(position).getParagraph());
        holder.number.setText(String.valueOf(list.get(position).getNumber()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
private TextView header,number,paragraph;
private RelativeLayout relativeLayout;
        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            header=itemView.findViewById(R.id.text_header);
            number=itemView.findViewById(R.id.text_no);
            paragraph=itemView.findViewById(R.id.text_paragraph);
            relativeLayout=itemView.findViewById(R.id.relative_single_box_home);
           gradientDrawable=(GradientDrawable) relativeLayout.getBackground();
        }
    }
}
