package com.ayata.commcare;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import java.security.AccessController;
import java.util.ArrayList;
import java.util.List;

public class SelectDesignation extends AppCompatActivity implements AdapterSelectDesignation.SingleClickListener{
    private List<ModelDesignation> list= new ArrayList<>();
private  AdapterSelectDesignation adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_designation);
        RecyclerView recyclerView=findViewById(R.id.recyclerid);
//       adapter = new AdapterSelectDesignation(list,this);
        adapter = new AdapterSelectDesignation(list);

        RecyclerView.LayoutManager manager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
adapter.setOnItemClickListener(this);
recyclerView.setAdapter(adapter);
        DataPrepare();
    }
    public  void  DataPrepare(){
list.add(new ModelDesignation("CHW 1"));
        list.add(new ModelDesignation("CHW 1 Naka"));
        list.add(new ModelDesignation("CHW 2"));
    }

    @Override
    public void onItemClickListener(int position) {
//        Drawable drawable=getResources().getDrawable(R.drawable.submit_button);
        adapter.selectedItem(position);
        Log.d("helo","selected item before" +position);


    }
}
